"use strict";

var rand = {'2':"images/foods/Burger.png",
'3':"images/foods/Pizza.png",
'1':"images/foods/twinkies.png",
'5':"images/foods/friedChicken.png",
'6':"images/foods/buffaloWings.png",
'7':"images/foods/CornDogs.png",
'8':"images/foods/Pastrami.png",
'9':"images/foods/bbqribs.png",
'4':"images/foods/ApplePie.png",
}
var randImage = {'0':["images/Reactions/sprite-200w_d (2)4f777cf3b01a2e4f9006711ba618cbf4.png",822,100,6],
'1':["images/Reactions/sprite-200w_d (4)5b8a8f53f3d0b80fd132455470f70785.png",574,120,4],
'2':["images/Reactions/200w_d (12).png",600,100,6],
'3':["images/Reactions/200w_d (11).png",1071 ,100,6],
'4':["images/Reactions/sprite-200w_d (5)86012e325475b7ed3f49076cbf43091a.png",900,150,6]
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var statusof = true;

var Game = function(canvasId){
    this.canvas = document.getElementById(canvasId);
    this.ctx = this.canvas.getContext('2d');
    this.entities = [];
}

Game.prototype.addEntity = function(entity){
    this.entities.push(entity);
}

Game.prototype.render = function () {
  //erase the page
  this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  for (var i = 0; i < this.entities.length; i++) {
    if (statusof === false && (i === 1 || i===0)) {
        console.log(this.entities[i]);
        this.entities[i].newDraw(this.ctx);
    }
    else{
        this.entities[i].draw(this.ctx);
    }
//    console.log(this.entities[1])
  }  
};

var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {isMobile = true};


var globalID;
var globalID2;

var Score = function (){
    this.playerScore = 0;           
    this.playerMiss = 0;
};
Score.prototype.newDraw = function (ctx) {

if(window.innerWidth<480){
    ctx.font='normal 1rem ProximaNova-Regular';
}else if(window.innerWidth<760){
    ctx.font='2rem ProximaNova-Regular';
}else{
    ctx.font='2rem ProximaNova-Regular';
}

//ctx.font = "2rem ProximaNova-Regular";
// if (isMobile == true){
//     console.log(isMobile);
//     ctx.fillStyle = 'black'; 
//     ctx.fillText("Press and hold Spacebar to shoot", window.innerWidth*.35, 380);
// }
//    ctx.font = "2rem Arial";
    ctx.fillStyle = 'black';
    ctx.fillText("Score: "+this.playerScore, 250, 150);

//    ctx.font = "2rem Arial";
    ctx.fillStyle = 'black';
    ctx.fillText("Miss: "+this.playerMiss, 250, 200);

};

Score.prototype.draw = function (ctx) {

if(window.innerWidth<480){
    ctx.font='1rem ProximaNova-Regular';
}else if(window.innerWidth<760){
    ctx.font='1.5rem ProximaNova-Regular';
}else{
    ctx.font='2rem ProximaNova-Regular';
}

//ctx.font = "2rem ProximaNova-Regular";
if (isMobile == false){
//    console.log(isMobile);
    ctx.fillStyle = 'red'; 
    ctx.fillText("Press and hold Spacebar to shoot", 220, 380);

if (this.playerMiss >= 3) {
    ctx.fillStyle = 'black';
    ctx.font='1.1rem ProximaNova-Regular'
    ctx.fillText("Reload to Start", 250, 150);
}
}
//    ctx.font = "2rem Arial";
    ctx.fillStyle = 'white';
    ctx.fillText("Score: "+this.playerScore, 20, 150);

//    ctx.font = "2rem Arial";
    ctx.fillStyle = 'white';
    ctx.fillText("Miss: "+this.playerMiss, 20, 200);

    if (this.playerMiss >= 30) {
//    ctx.font = "30px verdana";
    ctx.fillStyle = 'black';
    ctx.font='2.2rem ProximaNova-Regular'
    ctx.fillText("GAME OVER", 250, 50);
if (isMobile == true){
    ctx.fillStyle = 'black';
    ctx.font='1.1rem ProximaNova-Regular'
    ctx.fillText("Click on Image to start", 250, 150);
}

    statusof = "over";
    document.getElementById("authentic").style.display="block";
globalID = requestAnimationFrame(drawFlames);
//sleep(1000);
//location.reload();
} else {
cancelAnimationFrame(drawFlames);
}
};

//document.getElementById("authentic").style.display="none";

var AngleClass = function (x, y){
this.x = 110; //200
this.y = 310; //550
this.direction = -1; // Starts out moving      left
};

AngleClass.prototype.draw = function(ctx){
//start
ctx.beginPath();
// ctx.strokeStyle = 'white';
// var hand =  new Image();
// hand.src = "images/raised-hand-512.png"
// ctx.drawImage(hand,100,this.y);
ctx.strokeStyle = 'white';
ctx.lineWidth = 2.5;
ctx.moveTo(90, 330);
ctx.lineTo(this.x, this.y);
ctx.stroke();
ctx.closePath();

};

AngleClass.prototype.change = function(newX, newY){
this.y += newY * this.direction;
this.x += newX * this.direction; 
console.log(this.x, this.direction, this.y);
if (this.x < 107) {
// this.x += newX/2
this.direction = 0.5;
} else if(this.y > 320) {
this.direction = -0.5;
}
}

var Ball = function(x,y,radius, cntx){
this.x = x;
this.y = y;
this.radius = 0;
this.startAngle = 0;
this.endAngle = Math.PI*2;
this.xVel = 0;
this.yVel = 0;
this.made = false;
    this.cntx = cntx;
};
var random1;
Ball.prototype.draw = function (ctx){

var img2 = new Image();
//console.log(random1);
if (random1 == undefined){
img2.src = rand[1];
} else {
img2.src = rand[random1];
}
ctx.drawImage(img2,this.x,this.y)

};
   
Ball.prototype.shot = function (xVelocity,yVelocity){
if (statusof != "over" && statusof != false){
var self = this;
this.xVel = xVelocity;
this.yVel = yVelocity;

console.log(this.x,this.y);

var timer = setInterval(function () {
self.move(timer,newScore);

},20);
}
};
var c = 0;
Ball.prototype.move = function(timeVar,score) {
    if(!this.collide(newHoop)){ //doesn't collide with any object
    this.x+=this.xVel;
    this.y+=this.yVel;
    this.yVel+= 1; //gravity is 1

   if(this.x > canvas.width || this.y > canvas.height) {
    if( statusof){
    score.playerMiss++;
    }   
    this.reset(timeVar);
   }
    } else if (this.collide(newHoop) == 1){
score.playerScore ++;
console.log(score.playerScore);
statusof = false;   
    
this.reset(timeVar);

//    this.hoopAnimation();
    } else if (this.collide(newHoop) == 2){
    this.xVel = this.xVel * -1;
    // this.yVel = this.yVel * -1
    this.x += this.xVel-3;
this.y += this.yVel;
score.playerScore ++;
//this.hoopAnimation();

// if(this.x + this.radius <= hoop.hoopX - 90 && this.x + this.radius >= hoop.hoopX - 160 && this.y + this.radius >= hoop.hoopY + 125 && this.y + this.radius <= hoop.hoopY + 140) {
// this.made = true;
// return 1;
// }
    } else if (this.collide(newHoop) == 3){
    this.xVel = this.xVel * -1;
    this.yVel = this.yVel * -1;
    this.x += this.xVel;
this.y += this.yVel;
}
};
//console.log(statusof);
Ball.prototype.collide = function(hoop) {

if (this.x + this.radius >= hoop.hoopX - 60 && this.y >= hoop.hoopY - 20 && this.y <= hoop.hoopY + 100 && statusof == true) {
        // setTimeout(newImage(cntx,this.hoopx,this.hoopY),10);
       // setInterval(hoop.newDraw(curFrame4),10);
        //clearInterval(id);
       // console.log(hoop.hoopX,hoop.hoopY, this.x,this.y);
    //     if(this.y  - hoop.hoopY<= 70){ 
    //     this.y  =this.y + 100;
    // };

        hoop.stop();

return 1
}
};

Ball.prototype.hoopAnimation = function(timeVar){

 this.xVel = 0;
 this.yVel = 0
 this.y += this.yVel;
//     if(this.y > 550) {
this.reset(timeVar);
//    }
};

Ball.prototype.reset = function(timeVar){

this.x = 15;
this.y = 350;
random1 = getRandomInt(1,9)
//this.made = false;

// if (newScore.playerScore == 0){
// cancelAnimationFrame(drawFlames)
// }
//console.log(random1);

clearInterval(timeVar);

};
var ff;
//draws all pieces of the hoop
var Hoop = function (hoopX,hoopY,hoopEndX,hoopEndY, ctx,curFrame,curFrame2) {

this.hoopX = hoopX;
this.hoopY = hoopY;
this.hoopEndX = hoopEndX;
this.hoopEndY = hoopEndY;
    this.xVel = 0;
this.yVel = 0;
    this.ctx = ctx;
    this.prevY = this.hoopY;
    this.status = "down";
this.curFrame = curFrame;
this.curFrame2 = curFrame2;

};


Hoop.prototype.shot = function (xVelocity,yVelocity){
var self = this;
this.xVel = xVelocity;
this.yVel = yVelocity;

this.move();
};

Hoop.prototype.stop = function (){
};


Hoop.prototype.move = function(timeVar,score) {
    if (statusof == true){
   if(this.hoopY > 250) {
            this.status = "up";
    } 
    if (this.hoopY < 50){
                this.status = "down";
                 
            }
    this.prevY = this.hoopY;
    if (this.status === "up"){
        this.hoopY-=2;
    this.hoopEndY-=2  ;
    }else{
        this.hoopY+=2;
    this.hoopEndY+=2  ;
    }
   }
    
};

Hoop.prototype.newDraw = function (curFrame) {

var that = this;
var ctx = that.ctx;
if (random1 == undefined){
//character.src = rand[1];
ff = 1;
} else {
//character.src = rand[random1];
ff = Math.round(random1*0.49)
}
console.log(ff);

 var spriteWidth = randImage[ff][1]; 
 var spriteHeight = randImage[ff][2]; 
 
 //we are having two rows and 8 cols in the current sprite sheet
 var rows = 1; 
 var cols = randImage[ff][3]; 
 
 //The 0th (first) row is for the right movement
 var trackRight = 0; 
 
 //1st (second) row for the left movement (counting the index from 0)
 var trackLeft = 1; 
 
 //To get the width of a single sprite we divided the width of sprite with the number of cols
 //because all the sprites are of equal width and height 
 var width = spriteWidth/cols; 
 
 //Same for the height we divided the height with number of rows 
 var height = spriteHeight/rows; 
 
 //Each row contains 8 frame and at start we will display the first frame (assuming the index from 0)
// var curFrame2 = this.curFrame; 

 //The total frame is 8 
 var frameCount = 6; 
 
 //x and y coordinates to render the sprite 
 var x=0;
 var y=0; 
 
 //x and y coordinates of the canvas to get the single frame 
 var srcX=0; 
 var srcY=0; 
 
 //tracking the movement left and write 
 var left = false;
 
                        //Assuming that at start the character will move right side 
 var right = true;
 
 //Speed of the movement 
 var speed = 20; 
 
 // canvas.width = canvasWidth;
 // canvas.height = canvasHeight; 
 // //Creating an Image object for our character 
 var character = new Image(); 
 
 //Setting the source to the image file 
// character.src = "images/sprite-200w_d (2)4f777cf3b01a2e4f9006711ba618cbf4.png";
//character.src = "images/sprite-200w_d (9)82d5ad2af89b95972cf5acc936ea8dac.png"
//character.src = "images/sprite-200w_d (5)86012e325475b7ed3f49076cbf43091a.png";
// console.log(curFrame);
character.src = randImage[ff][0];
function updateFrame(curFrame,curFrame2){
 //Updating the frame index 
 curFrame2 = curFrame2 + 0.16;
 curFrame = curFrame + curFrame2;
 curFrame = Math.round(curFrame2) % frameCount; 
 if (curFrame2 > 8){
    curFrame2 = 0;

 }


// console.log(curFrame);
// console.log(curFrame2);
 
 //Calculating the x coordinate for spritesheet 
 srcX = curFrame * width; 
 ctx.clearRect(x,y,width,height);
 return [curFrame, curFrame2]
}
 var flag = updateFrame(this.curFrame, this.curFrame2);
 this.curFrame = flag[0];
 this.curFrame2 = flag[1];
 //Drawing the image 
 ctx.drawImage(character,srcX,srcY,width,height,this.hoopX-50,this.hoopY,width,height);
//}

}

function statuschange() {
    statusof = true;
}

setInterval(statuschange, 5000); 

Hoop.prototype.draw = function (ctx) {
 var spriteWidth =600; 
 var spriteHeight = 100; 
 
 //we are having two rows and 8 cols in the current sprite sheet
 var rows = 1; 
 var cols = 6; 
 
 //The 0th (first) row is for the right movement
 var trackRight = 0; 
 
 //1st (second) row for the left movement (counting the index from 0)
 var trackLeft = 1; 
 
 //To get the width of a single sprite we divided the width of sprite with the number of cols
 //because all the sprites are of equal width and height 
 var width = spriteWidth/cols; 
 
 //Same for the height we divided the height with number of rows 
 var height = spriteHeight/rows; 
 
 //Each row contains 8 frame and at start we will display the first frame (assuming the index from 0)
// var curFrame2 = this.curFrame; 

 //The total frame is 8 
 var frameCount = 6; 
 
 //x and y coordinates to render the sprite 
 var x=0;
 var y=0; 
 
 //x and y coordinates of the canvas to get the single frame 
 var srcX=0; 
 var srcY=0; 
 
 //tracking the movement left and write 
 var left = false;
 
                        //Assuming that at start the character will move right side 
 var right = true;
 
 //Speed of the movement 
 var speed = 20; 
 
 // canvas.width = canvasWidth;
 // canvas.height = canvasHeight; 
 // //Creating an Image object for our character 
 var character = new Image(); 
 
 //Setting the source to the image file 
 character.src = "images/sprite-200w_d (7)43b2b2e46ad056e12855f7bb2e30f7ec.png";
// character.src = "images/sprite-200w_d (9)35d2351f519602acee518a4c6eccc056.jpg";
function updateFrame(curFrame,curFrame2){
 //Updating the frame index 
 curFrame2 = curFrame2 + 0.16;
 curFrame = curFrame + curFrame2;
 curFrame = Math.round(curFrame2) % frameCount; 
 if (curFrame2 > 8){
    curFrame2 = 0;
 }


// console.log(curFrame);
// console.log(curFrame2);
 
 //Calculating the x coordinate for spritesheet 
 srcX = curFrame * width; 
 ctx.clearRect(x,y,width,height);
 return [curFrame, curFrame2]
}

//function draw(){
 //Updating the frame 
 var flag = updateFrame(this.curFrame, this.curFrame2);
 this.curFrame = flag[0];
 this.curFrame2 = flag[1];
 //Drawing the image 
 ctx.drawImage(character,srcX,srcY,width,height,this.hoopX-50,this.hoopY,width,height);
// ctx.drawImage(character,srcX,srcY,width,height,0,0,width,height);

//};

//setInterval(draw,100);

}


function sleep(miliseconds) {
   var currentTime = new Date().getTime();

   while (currentTime + miliseconds >= new Date().getTime()) {
   }
}


# Canvas-Game
HTML/Javascript game using Canvas API

## Technologies
* HTML
* CSS
* Javascript
* Canvas API

## Features
* Collision Detection
* Animated Sprites after making three shots in a row
* Shot power based on time
* Shot trajectory based on indicated angle

